// ��������� ��������.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "��������� ��������.h"

using namespace std;

#define MAX_LOADSTRING 100

// ���������� ����������:
HINSTANCE hInst;								// ������� ���������
TCHAR szTitle[MAX_LOADSTRING];					// ����� ������ ���������
TCHAR szWindowClass[MAX_LOADSTRING];			// ��� ������ �������� ����

struct point
{
	double x; 
	double y;
};
struct rectangle
{
	double w;
	double h;
	point p;
};
struct solution
{
	vector <int> sol;
	double fit;
};
struct fitness_cache
{
	double fit;
	vector <rectangle> rects;
};

const double EPS = 1e-3;
int genSize;
int mutRate;
vector <rectangle> rects;
int appMode;
vector <solution> gen;
solution bestSol;
double ground;
double wall;
double targetFit;
int crossRate;
int eliteRate;
int rot;
TCHAR fInName[100];
TCHAR fOutName[100];
int errCode = 0;
double scal;
int selForCrossMode = 3;
int crossMode = 0;
int selMutantsMode = 1;
int mutMode = 2;
int selMode = 1;
time_t startTime;
time_t endTime;
int timeLimit;
int testCount;
bool testMode;
double averageTime;
double averageFit;
double minFit;
double maxFit;
vector <double> testFits;
int iterCount = 0;
bool evolving;
bool showMidRes = true;
bool resShown;
bool testMessageShown = false;
bool randomExample;
bool gotScale = false;
int randomCount = 30;
unordered_map <string, fitness_cache> fit_caches;
int injectionSize = 10;
int stagnation_counter;
int max_iterations = 0;
HFONT font1 = CreateFont(50, 15, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, L"Times New Roman Cyr");
HBRUSH brush = CreateSolidBrush(RGB(0, 255, 0));
HPEN penBlack = CreatePen(0, 1, RGB(0, 0, 0));
HPEN penBlue = CreatePen(0, 3, RGB(0, 0, 255));
HBRUSH brushBlue = CreateSolidBrush(RGB(0, 0, 255));

// ��������� ���������� �������, ���������� � ���� ������ ����:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	StartDialog(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	appMode = 0;
	srand(time(NULL));
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	
 	// TODO: ���������� ��� �����.
	MSG msg;
	HACCEL hAccelTable;

	// ������������� ���������� �����
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MY, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// ��������� ������������� ����������:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY));

	
	BOOL bRet;
	// ���� ��������� ���������:
	while (bRet = GetMessage(&msg, NULL, 0, 0) != 0)
	{
		if (bRet == -1)
		{

		}
		else 
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return (int) msg.wParam;
}



//
//  �������: MyRegisterClass()
//
//  ����������: ������������ ����� ����.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MY);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   �������: InitInstance(HINSTANCE, int)
//
//   ����������: ��������� ��������� ���������� � ������� ������� ����.
//
//   �����������:
//
//        � ������ ������� ���������� ���������� ����������� � ���������� ����������, � �����
//        ��������� � ��������� �� ����� ������� ���� ���������.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // ��������� ���������� ���������� � ���������� ����������

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

string sol_to_str(vector <int> sol)
{
	string result;
	for (int i = 0; i < sol.size(); i++)
	{
		result += " " + to_string(sol[i]);
	}
	return result;
}
double areaOfIntersect(rectangle a, rectangle b)
{
	double dx, dy;
	dx = (a.p.x <= b.p.x) ? a.w - (b.p.x - a.p.x) : b.w - (a.p.x - b.p.x);
	dy = (a.p.y <= b.p.y) ? a.h - (b.p.y - a.p.y) : b.h - (a.p.y - b.p.y);
	if (dx < EPS)
		dx = 0;
	if (dy < EPS)
		dy = 0;
	return dx*dy;
}

double putRects(vector <int> sol)
{
	if (fit_caches.count(sol_to_str(sol)))
	{
		fitness_cache cache = fit_caches.at(sol_to_str(sol));
		rects = cache.rects;
		return cache.fit;
	}
	rects[sol[0]].p.x = 0;
	rects[sol[0]].p.y = 0;

	for (int i = 1; i < rects.size(); ++i)
	{

		vector <point> points(0);

		for (int j = 0; j < i; ++j)
		{
			point p = rects[sol[j]].p;
			p.x += rects[sol[j]].w;
			if (p.x + rects[sol[j]].w < wall && p.y + rects[sol[j]].h < ground)
				points.push_back(p);
			p = rects[sol[j]].p;
			p.y += rects[sol[j]].h;
			if (p.x + rects[sol[j]].w < wall && p.y + rects[sol[j]].h < ground)
				points.push_back(p);
			p.x += rects[sol[j]].w;
			if (p.x + rects[sol[j]].w < wall && p.y + rects[sol[j]].h < ground)
				points.push_back(p);
		}

		bool flag1 = points.empty();

		point pBest;
		double xBest = rects[sol[0]].w;
		double yBest = rects[sol[0]].h;
		for (int k = 1; k < i; ++k)
		{
			if (xBest < rects[sol[k]].p.x + rects[sol[k]].w)
				xBest = rects[sol[k]].p.x + rects[sol[k]].w;
			if (yBest < rects[sol[k]].p.y + rects[sol[k]].h)
				yBest = rects[sol[k]].p.y + rects[sol[k]].h;
		}

		double resBest = -1;
		bool f = false;
		for (int j = 0; j < points.size(); ++j)
		{ 
			double sum = 0;
			double resX, resY;
			rects[sol[i]].p = points[j];
			
				if (xBest < rects[sol[i]].p.x + rects[sol[i]].w)
					resX = rects[sol[i]].p.x + rects[sol[i]].w;
				else resX = xBest;
				if (yBest < rects[sol[i]].p.y + rects[sol[i]].h)
					resY = rects[sol[i]].p.y + rects[sol[i]].h;
				else resY = yBest;
				for (int k = 0; k < i; ++k)
					sum += areaOfIntersect(rects[sol[i]], rects[sol[k]]);
				if (sum < EPS)
				if (!f || resX*resY < resBest)
				{
					pBest = points[j];
					resBest = resX*resY;
					f = true;
				}


		}
		if (resBest == -1)
			flag1 = true;


		bool flag2 = false;
		point pBest1;
		if (!flag1)
			pBest1 = pBest;
		double resBest1 = resBest;
		if (!rot)
			flag2 = true;
		else
		{
			points.resize(0);
			
			if (!flag1)
				pBest1 = pBest;
			
			swap(rects[sol[i]].w, rects[sol[i]].h);


			for (int j = 0; j < i; ++j)
			{
				point p = rects[sol[j]].p;
				p.x += rects[sol[j]].w;
				if (p.x + rects[sol[j]].w < wall && p.y + rects[sol[j]].h < ground)
					points.push_back(p);
				p = rects[sol[j]].p;
				p.y += rects[sol[j]].h;
				if (p.x + rects[sol[j]].w < wall && p.y + rects[sol[j]].h < ground)
					points.push_back(p);
				p.x += rects[sol[j]].w;
				if (p.x + rects[sol[j]].w < wall && p.y + rects[sol[j]].h < ground)
					points.push_back(p);
			}

			flag2 = points.empty();

			xBest = rects[sol[0]].w;
			yBest = rects[sol[0]].h;
			for (int k = 1; k < i; ++k)
			{
				if (xBest < rects[sol[k]].p.x + rects[sol[k]].w)
					xBest = rects[sol[k]].p.x + rects[sol[k]].w;
				if (yBest < rects[sol[k]].p.y + rects[sol[k]].h)
					yBest = rects[sol[k]].p.y + rects[sol[k]].h;
			}

			resBest = -1;
			f = false;
			for (int j = 0; j < points.size(); ++j)
			{
				double sum = 0;
				double resX, resY;
				rects[sol[i]].p = points[j];

				if (xBest < rects[sol[i]].p.x + rects[sol[i]].w)
					resX = rects[sol[i]].p.x + rects[sol[i]].w;
				else resX = xBest;
				if (yBest < rects[sol[i]].p.y + rects[sol[i]].h)
					resY = rects[sol[i]].p.y + rects[sol[i]].h;
				else resY = yBest;
				for (int k = 0; k < i; ++k)
					sum += areaOfIntersect(rects[sol[i]], rects[sol[k]]);
				if (sum < EPS)
				if (!f || resX*resY < resBest)
				{
					pBest = points[j];
					resBest = resX*resY;
					f = true;
				}


			}
			if (resBest == -1)
				flag2 = true;
		}
		if (!flag1 && !flag2)
		{
			if (resBest1 < resBest)
			{
				swap(rects[sol[i]].w, rects[sol[i]].h);
				pBest = pBest1;
			}
			rects[sol[i]].p = pBest;
		}
		else
		{
			if (flag1 && flag2)
				return 0;
			if (!flag1)
			{
				rects[sol[i]].p = pBest1;
				if (rot)
					swap(rects[sol[i]].w, rects[sol[i]].h);
			}
			else
			if (!flag2)
				rects[sol[i]].p = pBest;
		}
	}
	double dx = rects[sol[0]].p.x + rects[sol[0]].w;
	double dy = rects[sol[0]].p.y + rects[sol[0]].h;
	double sum = 0;
	for (int i = 0; i < rects.size(); ++i)
	{
		if (rects[sol[i]].p.x + rects[sol[i]].w > dx)
			dx = rects[sol[i]].p.x + rects[sol[i]].w;
		if (rects[sol[i]].p.y + rects[sol[i]].h > dy)
			dy = rects[sol[i]].p.y + rects[sol[i]].h;
		sum += rects[sol[i]].w*rects[sol[i]].h;
	}
	double result = sum / (dx*dy);
	fitness_cache new_cache = { result , rects};
	fit_caches.insert({ sol_to_str(sol), new_cache });
	return result;
}

bool compGen(solution a, solution b)
{
	return a.fit > b.fit;
}

void readRects()
{
	if (randomCount <= 0)
	{
		errCode = 13;
		return;
	}
	if (timeLimit <= 0)
	{
		errCode = 11;
		return;
	}
	if ((testCount <= 0) && testMode)
	{
		errCode = 12;
		return;
	}
	if (ground <= 0 || wall <= 0)
	{
		errCode = 2;
		return;
	}
	
	if (targetFit >= 1 || targetFit <= 0)
	{
		errCode = 4;
		return;
	}
	if (genSize <= 0)
	{
		errCode = 5;
		return;
	}
	if (mutRate > genSize || mutRate < 0)
	{
		errCode = 6;
		return;
	}
	if (crossRate <= 0 || crossRate > genSize)
	{
		errCode = 9;
		return;
	}
	if (eliteRate <= 0 || eliteRate > genSize)
	{
		errCode = 10;
		return;
	}

	if (!randomExample)
	{
		fstream fIn(fInName, ios::in);
		if (!fIn.is_open())
		{
			errCode = 1;
			return;
		}
		rects.clear();
		while (!fIn.eof())
		{
			rectangle x;
			fIn >> x.w >> x.h;
			rects.push_back(x);
			if (x.w < 0 || x.h < 0)
			{
				errCode = 3;
				return;
			}
		}
		fIn.close();
	}
	else
	{
		srand(time(NULL));
		rects.clear();
		for (int i = 0; i < randomCount; i++)
		{
			rectangle r;
			r.h = rand() % 60 + 20;
			r.w = rand() % 90 + 10;
			rects.push_back(r);
		}
	}
	appMode = 2;
}

int distHamm(solution s1, solution s2)
{
	int d = 0;
	for (int i = 0; i < rects.size(); i++)
		if (s1.sol[i] != s2.sol[i])
			d++;
		return d;
}
// ����� ������ ��� �����������
vector <int> SFCpanmixia()					
{
	vector <int> couples(gen.size());
	for (int j = 0; j < gen.size(); ++j)		// �������� ���� �������� ��� �������������
		couples[j] = j;
	for (int j = 0; j < gen.size(); ++j)
	{
		int i1;
		do
		{
			i1 = rand() % gen.size();
		} while (gen[j].fit == gen[couples[i1]].fit || gen[i1].fit == gen[couples[j]].fit);
		swap(couples[i1], couples[j]);
	}
	return couples;
}

int minHamm(int j)
{
	vector <int> dists(genSize);
	bool f = false;
	for (int i = 0; i < genSize; i++)
	{
		dists[i] = distHamm(gen[j], gen[i]);
		if (dists[i] != 0)
			f = true;
	}
	if (!f)
		return 0;

	int min;
	for (int i = 0; i < genSize;++i)
	if (dists[i] != 0)
	{
		min = i;
		break;
	}

	for (int i = 0; i < genSize; i++)
	if ((dists[i] < dists[min]) && (dists[i] != 0))
		min = i;

	return min;

}

int maxHamm(int j)
{
	vector <int> dists(genSize);
	for (int i = 0; i < genSize; i++)
		dists[i] = distHamm(gen[j], gen[i]);

	int max = 0;

	for (int i = 1; i < genSize; i++)
	if ((dists[i] > dists[max]))
		max = i;

	return max;

}

int minFitDiff(int j)
{
	vector <double> dists(genSize);
	bool f = false;
	for (int i = 0; i < genSize; i++)
	{
		dists[i] = abs(gen[j].fit - gen[i].fit);
		if (dists[i] != 0.00)
			f = true;
	}
	if (!f)
		return 0;

	int min;
	for (int i = 0; i < genSize; ++i)
	if (dists[i] != 0.00)
	{
		min = i;
		break;
	}

	for (int i = 0; i < genSize; i++)
	if ((dists[i] < dists[min]) && (dists[i] != 0))
		min = i;

	return min;
}

vector <int> SFCinbreeding()
{
	vector <int> couples(gen.size());
	for (int j = 0; j < gen.size(); ++j)		// �������� ���� �������� ��� �������������
		couples[j] = minHamm(j);
	
	return couples;
}

vector <int> SFCoutbreeding()
{
	vector <int> couples(gen.size());
	for (int j = 0; j < gen.size(); ++j)		// �������� ���� �������� ��� �������������
		couples[j] = maxHamm(j);

	return couples;
}

vector <int> SFCassociative()
{
	vector <int> couples(gen.size());
	for (int j = 0; j < gen.size(); ++j)		// �������� ���� �������� ��� �������������
		couples[j] = minFitDiff(j);

	return couples;
}
// �����������
void crossOX(vector <int> couples)
{
	vector <bool> couples_used(couples.size());
	for (int k = 0; k < crossRate / 2; ++k)		// ������������
	{
		int j;
		do
		{
			j = rand() % couples.size();
		} while (couples_used[j]);
		couples_used[j] = true;
		couples_used[couples[j]] = true;
		solution child1, child2, s1, s2;
		s1 = gen[couples[j]];
		s2 = gen[j];
		int cutPoint = rand() % rects.size();
		for (int i = 0; i < cutPoint; ++i)
		{
			child1.sol.push_back(gen[j].sol[i]);
			child2.sol.push_back(gen[couples[j]].sol[i]);
			for (int t = 0; t < rects.size(); ++t)
			if (s1.sol[t] == gen[j].sol[i])
			{
				s1.sol.erase(s1.sol.begin() + t);
				break;
			}
			for (int t = 0; t < rects.size(); ++t)
			if (s2.sol[t] == gen[couples[j]].sol[i])
			{
				s2.sol.erase(s2.sol.begin() + t);
				break;
			}
		}
		for (int i = cutPoint; i < rects.size(); ++i)
		{
			child1.sol.push_back(s1.sol[i - cutPoint]);
			child2.sol.push_back(s2.sol[i - cutPoint]);
		}

		child1.fit = putRects(child1.sol);
		child2.fit = putRects(child2.sol);
		gen.push_back(child1);
		gen.push_back(child2);
	}
}

int findDig(solution s, int k)
{
	for (int i = 0; i < s.sol.size();i++)
	if (s.sol[i] == k)
		return i;
}

void crossShuffle(vector <int> couples)
{
	for (int k = 0; k < crossRate / 2; ++k)
	{
		int j = rand() % genSize;
		solution child1, child2;
		child1 = gen[j];
		child2 = gen[couples[j]];
		int m = rand() % (rects.size()/2);
		for (int i = 0; i < m; i++)
		{
			int t = rand() % rects.size();
			swap(child1.sol[t], child2.sol[findDig(child2, child1.sol[t])]);
		}
		child1.fit = putRects(child1.sol);
		child2.fit = putRects(child2.sol);
		gen.push_back(child1);
		gen.push_back(child2);
	}
	
}
// ����� ������ ��� �������
vector <int> selMutantsRand()
{
	vector <int> mutants;					// �������� ��������
	for (int i = 0; i < mutRate; ++i)
		mutants.push_back(rand() % gen.size());
	
	return mutants;
}

vector <int> selMutantsBest()
{
	vector <int> mutants;					// �������� ��������
	sort(gen.begin(), gen.end(),compGen);
	for (int i = 0; i < mutRate; ++i)
		mutants.push_back(i);

	return mutants;
}

vector <int> selMutantsWorst()
{
	vector <int> mutants;					// �������� ��������
	sort(gen.begin(), gen.end(),compGen);
	for (int i = 0; i < mutRate; ++i)
		mutants.push_back(gen.size()-i-1);

	return mutants;
}
// �������
void mutateOnePoint(vector <int> mutants)
{
	for (int i = 0; i < mutRate; ++i)		// �������
	{
		solution child = gen[mutants[i]];
		int cutPoint = rand() % (rects.size() - 1);
		swap(child.sol[cutPoint], child.sol[cutPoint + 1]);
		child.fit = putRects(child.sol);
		gen.push_back(child);
	}
}

void mutateInverse(vector <int> mutants)
{
	for (int i = 0; i < mutRate; ++i)		// �������
	{
		solution child = gen[mutants[i]];
		int cutPoint1 = rand() % (rects.size() - 1);
		int cutPoint2 = cutPoint1 + rand() % (rects.size() - 1 - cutPoint1);
		
		reverse(child.sol.begin() + cutPoint1, child.sol.begin() + cutPoint2);
		child.fit = putRects(child.sol);
		gen.push_back(child);
	}
}

void mutateSaltation(vector <int> mutants)
{
	for (int i = 0; i < mutRate; ++i)		// �������
	{
		solution child = gen[mutants[i]];
		vector <int> sal1((rects.size() / 10) + 1);
		vector <int> sal2((rects.size() / 10) + 1);
		for (int j = 0; j < sal1.size(); j++)
		{
			sal1[j] = rand() % rects.size();
			sal2[j] = rand() % rects.size();
		}

		for (int j = 0; j < sal1.size(); j++)
			swap(child.sol[sal1[j]], child.sol[sal2[j]]);

		child.fit = putRects(child.sol);
		gen.push_back(child);
	}
}
// ����� � ����� ���������
vector <solution> eliteSel()
{
	sort(gen.begin(), gen.end(), compGen);	// ���������� ��������� ��� ������
	vector <solution> newGen;

	int elite_counter = 1;
	int i = 1;
	newGen.push_back(gen[0]);
	while (elite_counter < eliteRate && i < gen.size())
	{
		if (gen[i].fit != gen[i - 1].fit)
		{
			elite_counter++;
			newGen.push_back(gen[i]);
		}
		i++;
	}

	for (int j = eliteRate; j < genSize; j++)
	{
		newGen.push_back(gen[(rand() % gen.size() - i) + i]);
	}

	return newGen;
}

vector <solution> tournamentSel()
{
	vector <solution> newGen;
	for (int i = 0; i < genSize; ++i)
	{
		int a, b;
		do
		{
			a = rand() % genSize;
			b = rand() % genSize;
		} while (a == b);

		if (compGen(gen[a], gen[b]))
			newGen.push_back(gen[a]);
		else
			newGen.push_back(gen[b]);
	}
	return newGen;
}

solution local_descent(solution sol_arg)
{
	solution best_sol = sol_arg;
again:
	for (int i = 0; i < rects.size(); i++)
	for (int j = i + 1; j < rects.size(); j++)
	{
		solution new_sol = best_sol;
		swap(new_sol.sol[i], new_sol.sol[j]);
		new_sol.fit = putRects(new_sol.sol);
		if (new_sol.fit > best_sol.fit)
		{
			best_sol = new_sol;
			if (startTime + timeLimit < time(NULL) || targetFit <= best_sol.fit)
			{
				evolving = false;
				return best_sol;
			}
			goto again;
		}
	}
	return best_sol;
}

void evolve()
{
	if (!evolving)
	{
		bestSol.fit = 0;
		startTime = time(NULL);
		if (testMode)
			testFits.clear();
		iterCount = 0;
		gen.resize(genSize);
		srand(time(NULL));
		for (int i = 0; i < genSize; ++i)			// ��������� ������� ��������� ���������� ���������
		{
			gen[i].sol.resize(rects.size());
			for (int j = 0; j < rects.size(); ++j)
				gen[i].sol[j] = j;
			for (int j = 0; j < rects.size(); ++j)
				swap(gen[i].sol[rand() % rects.size()], gen[i].sol[rand() % rects.size()]);
		}
		sort(gen[0].sol.begin(), gen[0].sol.end());
		for (int i = 0; i < genSize; ++i)
			gen[i].fit = putRects(gen[i].sol);
		evolving = true;

	}
			do											// ������� ��������
			{
				srand(time(NULL));

				int clones_of_best_counter = 0;

				for (int i = 0; i < gen.size(); ++i)
				{
					if (gen[i].fit == bestSol.fit)
						clones_of_best_counter++;
				}

				if (clones_of_best_counter > genSize / 10 * 2 || stagnation_counter > 8)
				{
					genSize = genSize;
					for (int i = 0; i < injectionSize; i++)
					{
						solution new_sol;
						new_sol.sol.resize(rects.size());
						for (int j = 0; j < rects.size(); ++j)
							new_sol.sol[j] = j;
						for (int j = 0; j < rects.size(); ++j)
							swap(new_sol.sol[rand() % rects.size()], new_sol.sol[rand() % rects.size()]);
						new_sol.fit = putRects(new_sol.sol);
						gen.push_back(new_sol);
					}
				}

				if (stagnation_counter > 8)
					gen.push_back(local_descent(gen[0]));

				vector <int> couples;
				switch (selForCrossMode)				// ����� ��� ��� ����������
				{
					case 0: couples = SFCpanmixia();
						break;
					case 1: couples = SFCinbreeding();
						break;
					case 2: couples = SFCoutbreeding();
						break;
					case 3: couples = SFCassociative();
						break;
				}

				switch (crossMode)						// ���������
				{
					case 0: crossOX(couples);
						break;
					case 1: crossShuffle(couples);
						break;
				}

				vector <int> mutants;
				switch (selMutantsMode)					// ����� ��������
				{
					case 0: mutants = selMutantsRand();
						break;
					case 1: mutants = selMutantsBest();
						break;
					case 2: mutants = selMutantsWorst();
						break;
				}

				switch (mutMode)						// �������
				{
					case 0: mutateOnePoint(mutants);
						break;
					case 1: mutateInverse(mutants);
						break;
					case 2: mutateSaltation(mutants);
						break;

				}

				switch (selMode)						// ����� � ����� ���������
				{
					case 0: gen = eliteSel();
						break;
					case 1: gen = tournamentSel();
						break;
				}
				
				if (gen[0].fit == bestSol.fit)
					stagnation_counter++;
				else
					stagnation_counter = 0;
				bestSol = gen[0];						// ����� ���������� ������� � ������� ��������� 
				for (int i = 1; i < genSize; ++i)
				if (bestSol.fit < gen[i].fit)
					bestSol = gen[i];

				putRects(bestSol.sol);

				if (bestSol.fit == 0)
				{
					evolving = false;
					break;
				}

				if (startTime + timeLimit < time(NULL))
				{
					evolving = false;
					break;
				}

				if (testMode)
					testFits.push_back(bestSol.fit);
					
				iterCount++;

				if (max_iterations > 0 && iterCount >= max_iterations)
				{
					evolving = false;
					break;
				}

				if (targetFit <= bestSol.fit)
				{
					evolving = false;
					break;
				}
				if (!testMode)
					return;
			} while (true);

			endTime = time(NULL);
			putRects(bestSol.sol);
			if (bestSol.fit == 0)
			{
				appMode = 0;
				errCode = 7;
			}
			else
			{
				appMode = 3;	
			}
}

void writeRects()
{
	fstream fOut(fOutName, ios::out);
	for (int i = 0; i < rects.size(); ++i)
	{
		fOut << rects[i].p.x << ' ' << rects[i].p.y << ' ' << rects[i].w << ' ' << rects[i].h << '\n';
	}
	if (testMode)
		fOut << "\n������� �����: " << averageTime << " ���\n���������� �������� �������: " << minFit << "\n������� �������� �������: " << averageFit << "\n���������� �������� �������: " << maxFit;
	else
		fOut << "\n�����: " << difftime(endTime, startTime) << " ���\n�������� �������: " << bestSol.fit;

	
	fOut.close();
}

LPCWSTR getErrMessage(int ec)
{
	switch (ec)
	{
	case 1:
		return L"���� �� ������";
		break;
	case 2:
		return L"������ ������������ ����������� �� �����������";
		break;
	case 3:
		return L"������������ ������ � �����";
		break;
	case 4:
		return L"������ ������������ �������� ������� �������";
		break;
	case 5:
		return L"����� ������������ ������ ���������";
		break;
	case 6:
		return L"������ ������������ ����� �������";
		break;
	case 7:
		return L"�� ������� ����� �������";
		break;
	case 8:
		return L"������� �� �����";
			break;
	case 9:
		return L"������ ������������ ����� �����������";
		break;
	case 10:
		return L"����� ������������ �������� ���������� ������";
		break;
	case 11:
		return L"������������ ����������� �� ����� ������";
		break;
	case 12:
		return L"������������ ����� �������� ��������";
		break;
	case 13:
		return L"������������ ����� ��������������� ��� ��������� �������";
		break;
	}
	return L"����������� ��� ������";
}

double getScale(double h)
{
	double max = 0;
	for (int i = 1; i < rects.size(); ++i)
	{
		if (rects[i].p.x + rects[i].w > max)
			max = rects[i].p.x + rects[i].w;
		if (rects[i].p.y + rects[i].h > max)
			max = rects[i].p.y + rects[i].h;
	}

	return (h * 0.75) / (max);
}

//
//  �������: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ����������:  ������������ ��������� � ������� ����.
//
//  WM_COMMAND	- ��������� ���� ����������
//  WM_PAINT	- ��������� ������� ����
//  WM_DESTROY	 - ������ ��������� � ������ � ���������.
//
//

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	GetWindowRect(hWnd, &rect);
	int wWidth = rect.right - rect.left;
	int wHeight = rect.bottom - rect.top;

	if (errCode)
	{
		LPCWSTR msg = getErrMessage(errCode);
		errCode = 0;
		appMode = 0;
		MessageBox(hWnd, msg, L"������", MB_ICONERROR);
	}

	switch (appMode)
	{
	case 0:
		evolving = false;
		resShown = true;
		testMessageShown = false;
		break;
	case 1:
		gotScale = false;
		readRects();
		break;
	case 2:
		if (!testMessageShown && testMode)
		{
			InvalidateRect(hWnd, NULL, true);
		}
		else
		{
			if (resShown)
			{
				evolve();
				if (!gotScale)
				{
					scal = getScale((double)(min(wHeight, wWidth)));
					gotScale = true;
				}
				resShown = false;
			}
			InvalidateRect(hWnd, NULL, true);
			if (testMode)
			{
				averageTime = difftime(endTime, startTime);
				averageFit = bestSol.fit;
				minFit = bestSol.fit;
				maxFit = bestSol.fit;
				for (int i = 1; i < testCount; i++)
				{
					fit_caches.clear();
					evolve();
					stagnation_counter = 0;
					if (bestSol.fit > maxFit)
						maxFit = bestSol.fit;
					if (bestSol.fit < minFit)
						minFit = bestSol.fit;
					averageTime += difftime(endTime, startTime);
					averageFit += bestSol.fit;
				}
				testMessageShown = false;
				averageTime /= testCount;
				averageFit /= testCount;
			}
		}
		break;
	case 3:
		InvalidateRect(hWnd, NULL, true);
		writeRects();
		appMode = 4;
		scal = getScale((double)(min(rect.bottom - rect.top,rect.right-rect.left)));
		InvalidateRect(hWnd, NULL, true);

		wchar_t str[256];
		if (testMode)
		{
			swprintf(str, L"������� �����: %f ���\n���������� �������� �������: %f\n������� �������� �������: %f\n���������� �������� �������: %f", averageTime, minFit, averageFit, maxFit);
			MessageBox(hWnd, str, L"������������ ���������!", MB_OK);
		}
		else
		{
			swprintf(str, L"�����: %i ���\n�������� �������: %f", (int)difftime(endTime, startTime), bestSol.fit);
			MessageBox(hWnd, str, L"������!", MB_OK);
		}
		break;
	}

	switch (message)
	{
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// ��������� ����� � ����:
		switch (wmId)
		{
		case IDD_OPENFILE:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_START), hWnd, StartDialog);
			break;

		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;

	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);

		switch (appMode)
		{
		case 0:
			break;
		case 1:
			break;
		case 2:
			if (testMode)
			{
				SelectObject(hdc, font1);
				SetTextColor(hdc, RGB(0, 0, 0));
				SetBkColor(hdc, RGB(255, 255, 255));
					TextOut(hdc, wWidth/2 - 150, wHeight*3/8, L"������������...", 15);
					testMessageShown = true;
			}
			else
			{
				if (evolving)
				{
					SelectObject(hdc, penBlack);
					SelectObject(hdc, brush);
					GetStockObject(SYSTEM_FONT);
					wchar_t str[20];
					wchar_t str2[20];
					swprintf(str, L"%2.10f", bestSol.fit);
					swprintf(str2, L"%8d", iterCount);
					TextOut(hdc, wWidth*5/8, wHeight*5/8, L"������� �������:", 16);
					TextOut(hdc, wWidth * 5 / 8 + 125, wHeight * 5 / 8, str,12);
					TextOut(hdc, wWidth * 5 / 8, wHeight * 5 / 8 + 20, L"����� ��������:", 15);
					TextOut(hdc, wWidth * 5 / 8 + 115, wHeight * 5 / 8 + 20, str2, 8);
					if (stagnation_counter > 8)
						TextOut(hdc, wWidth * 5 / 8, wHeight * 5 / 8 - 40, L"����������� ��������� �����", 27);
					for (int i = 0; i < rects.size(); ++i)
						Rectangle(hdc, floor(scal*rects[i].p.x), floor(scal*(rects[i].p.y + rects[i].h)), floor(scal*(rects[i].p.x + rects[i].w)), floor(scal*rects[i].p.y));
					resShown = true;
				}
			}
			break;
		case 3:
			break;
		case 4:
			
			SelectObject(hdc, penBlack);
			SelectObject(hdc, brush);
			GetStockObject(SYSTEM_FONT);
			if (testMode)
			{
				SelectObject(hdc, brushBlue);
				int step = (wWidth-100) / iterCount;
				MoveToEx(hdc, 50, wHeight - 100, NULL);
				LineTo(hdc, wWidth-1, wHeight - 100);
				MoveToEx(hdc, 50, wHeight - 100, NULL);
				LineTo(hdc, 50, 0);
				MoveToEx(hdc, 40, 100, NULL);
				LineTo(hdc, 60, 100);
				TextOut(hdc, 55, 100, L"1", 2);
				TextOut(hdc, wWidth - 150, wHeight - 80, L"����� ��������", 14);
				TextOut(hdc, 60, 30, L"������� �������", 15);
				TextOut(hdc, 55, 100, L"1", 2);
				for (int i = 0; i <= iterCount; ++i)
				{
					wchar_t str[10];
					swprintf(str, L"%i", i);
					MoveToEx(hdc, 50 + step*i, wHeight - 110, NULL);
					LineTo(hdc, 50 + step*i, wHeight-90);
					TextOut(hdc, 45 + step*i, wHeight - 95, str, 2);
				}
				point p;
				wchar_t str[10];
				swprintf(str, L"%f", testFits[0]);
				p.x = 50 + step;
				p.y = 100 + wHeight - testFits[0] * (wHeight);
				TextOut(hdc, p.x - 20, p.y - 20, str, 5);
				SelectObject(hdc,penBlue);
				for (int i = 1; i <= iterCount-1; ++i)
				{
					MoveToEx(hdc, p.x, p.y,NULL);
					Ellipse(hdc, p.x - 2, p.y -2,p.x+2,p.y+2);
					p.x += step;
					p.y = 100 + wHeight - testFits[i] * (wHeight);
					LineTo(hdc, p.x, p.y);
					swprintf(str, L"%f", testFits[i]);
					TextOut(hdc, p.x-20, p.y-20, str, 5);
				}
				Ellipse(hdc, p.x - 2, p.y - 2, p.x + 2, p.y + 2);
			}
			else
			for (int i = 0; i < rects.size(); ++i)
				Rectangle(hdc, floor(scal*rects[i].p.x), floor(scal*(rects[i].p.y + rects[i].h)), floor(scal*(rects[i].p.x + rects[i].w)), floor(scal*rects[i].p.y));
			resShown = true;
			break;
		}

		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	
	

	
	return 0;
}

INT_PTR CALLBACK StartDialog(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	TCHAR STR[20];
	BOOL succ = true;

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		SetDlgItemText(hDlg, ID_IFILE, L"D:/inp.txt");
		SetDlgItemText(hDlg, ID_OFILE, L"out.txt");
		SetDlgItemText(hDlg, ID_FIT, L"0.95");
		SetDlgItemInt(hDlg, IDC_TIMELIMIT, 30, true);
		SetDlgItemInt(hDlg, ID_WALL, 500, true);
		SetDlgItemInt(hDlg, ID_GROUND, 500, true);
		SetDlgItemInt(hDlg, ID_GENSIZE, 30, true);
		SetDlgItemInt(hDlg, ID_MUTRATE, 2, true);
		SetDlgItemInt(hDlg, ID_CROSSRATE, 30, true);
		SetDlgItemInt(hDlg, ID_ELITERATE, 5, true);
		CheckDlgButton(hDlg, ID_ROTATE, BST_CHECKED);
		CheckDlgButton(hDlg, IDC_PANMIXIA, BST_CHECKED);
		CheckDlgButton(hDlg, IDC_RANDOMMUT, BST_CHECKED);
		CheckDlgButton(hDlg, IDC_ONEPOINTMUT, BST_CHECKED);
		CheckDlgButton(hDlg, IDC_ELITESEL, BST_CHECKED);
		SetDlgItemInt(hDlg, IDC_TESTCOUNT, 200, true);
		SetDlgItemInt(hDlg, IDC_RANDOMCOUNT, 30, true);
		SetDlgItemInt(hDlg, IDC_ITERLIMIT, 200, true);
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDSTART)
		{
				rot = IsDlgButtonChecked(hDlg, ID_ROTATE);
				//showMidRes = IsDlgButtonChecked(hDlg, IDC_SHOWMIDRES);
				if (IsDlgButtonChecked(hDlg, IDC_PANMIXIA)) selForCrossMode = 0;
				if (IsDlgButtonChecked(hDlg, IDC_INBREEDING)) selForCrossMode = 1;
				if (IsDlgButtonChecked(hDlg, IDC_OUTBREEDING)) selForCrossMode = 2;
				if (IsDlgButtonChecked(hDlg, IDC_ASSOCIATIVE)) selForCrossMode = 3;


				if (IsDlgButtonChecked(hDlg, IDC_RANDOMMUT)) selMutantsMode = 0;
				if (IsDlgButtonChecked(hDlg, IDC_BESTMUT)) selMutantsMode = 1;
				if (IsDlgButtonChecked(hDlg, IDC_WORSTMUT)) selMutantsMode = 2;

				if (IsDlgButtonChecked(hDlg, IDC_ONEPOINTMUT)) mutMode = 0;
				if (IsDlgButtonChecked(hDlg, IDC_INVERSEMUT)) mutMode = 1;
				if (IsDlgButtonChecked(hDlg, IDC_SALTATIONMUT)) mutMode = 2;

				if (IsDlgButtonChecked(hDlg, IDC_ELITESEL)) selMode = 0;
				if (IsDlgButtonChecked(hDlg, IDC_TOURNAMENTSEL)) selMode = 1;

				testMode = IsDlgButtonChecked(hDlg, IDC_TESTMODE);
				randomExample = IsDlgButtonChecked(hDlg, IDC_RANDOMEXAMPLE);
				
				genSize = GetDlgItemInt(hDlg, ID_GENSIZE, &succ, TRUE);
				mutRate = GetDlgItemInt(hDlg, ID_MUTRATE, &succ, TRUE);
				eliteRate = GetDlgItemInt(hDlg, ID_ELITERATE, &succ, TRUE);
				crossRate = GetDlgItemInt(hDlg, ID_CROSSRATE, &succ, TRUE);
				timeLimit = GetDlgItemInt(hDlg, IDC_TIMELIMIT, &succ, TRUE);
				max_iterations = GetDlgItemInt(hDlg, IDC_ITERLIMIT, &succ, TRUE);

				if (randomExample)
				randomCount = GetDlgItemInt(hDlg, IDC_RANDOMCOUNT, &succ, TRUE);

				if (testMode)
				testCount = GetDlgItemInt(hDlg, IDC_TESTCOUNT, &succ, TRUE);
				try{
					GetDlgItemText(hDlg, ID_GROUND, STR, 20);
					ground = stod(STR);
					GetDlgItemText(hDlg, ID_WALL, STR, 20);
					wall = stod(STR);
					GetDlgItemText(hDlg, ID_FIT, STR, 20);
					targetFit = stod(STR);
					}
				catch (const std::invalid_argument& ia)
				{
					EndDialog(hDlg, LOWORD(wParam));
					errCode = 8;
					return (INT_PTR)TRUE;
				}

				double sumSquare = 0;
				for (int i = 0; i < rects.size(); i++)
				{
					sumSquare += rects[i].h * rects[i].w;
				}
				if (sumSquare > ground * wall)
				{
					EndDialog(hDlg, LOWORD(wParam));
					errCode = 2;
					return (INT_PTR)TRUE;
				}

			if (!succ)
			{
				EndDialog(hDlg, LOWORD(wParam));
				errCode = 8;
				return (INT_PTR)TRUE;
			}
			
			GetDlgItemText(hDlg, ID_IFILE, fInName, 100);
			GetDlgItemText(hDlg, ID_OFILE, fOutName, 100);
			appMode = 1;
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}

		if (LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}	
	}
	return (INT_PTR)FALSE;
}