//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется Двумерная упаковка.rc
//
#define IDC_MYICON                      2
#define IDD_MY_DIALOG                   102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MY                          107
#define IDI_SMALL                       108
#define IDC_MY                          109
#define IDR_MAINFRAME                   128
#define IDD_START                       129
#define ID_IFILE                        1000
#define ID_OFILE                        1004
#define ID_FIT                          1005
#define ID_WALL                         1006
#define ID_MUTRATE                      1007
#define ID_GENSIZE                      1008
#define IDSTART                         1009
#define ID_GROUND                       1010
#define ID_GENSIZE2                     1011
#define ID_CROSSRATE                    1011
#define ID_ELITERATE                    1012
#define ID_ROTATE                       1013
#define IDC_PANMIXIA                    1014
#define IDC_TESTMODE                    1015
#define IDC_INBREEDING                  1016
#define IDC_ASSOCIATIVE                 1017
#define IDC_TIMELIMIT                   1021
#define IDC_OUTBREEDING                 1022
#define IDC_TESTCOUNT                   1023
#define IDC_ITERLIMIT                   1024
#define IDC_RANDOMEXAMPLE               1025
#define IDC_RANDOMMUT                   1026
#define IDC_BESTMUT                     1027
#define IDC_WORSTMUT                    1028
#define IDC_ONEPOINTMUT                 1029
#define IDC_SALTATIONMUT                1030
#define IDC_INVERSEMUT                  1031
#define IDC_ELITESEL                    1032
#define IDC_TOURNAMENTSEL               1033
#define IDC_EDIT1                       1034
#define IDC_RANDOMCOUNT                 1034
#define ID_32771                        32771
#define ID_OPENFILE                     32772
#define IDD_OPENFILE                    32773
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
